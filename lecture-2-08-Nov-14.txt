��� ������� �������: google "�����������, ����������� ���� � �����",
��������� ����� ���������:
1) ��� �������� + ����, ������, �� ������������;
2) ����������� ����;
3) ��������� �����: ��� rvalue, ����������� ������ � ����� ����������, �� ����� ������ � ����� ��������.
    �������� ��� ���� ��� ���� ����, � ������� ���� ��������� �������, ������� �� ����� ���� � ����� ��������.
    ���������� ������������� ����� ����� �������� ���������.
    
������������ ��������� ��������

    �������� �������:
        �������� ��������� -- hoard-SLAB allocator;
        ���� malloc, free (� �� ������ ���� posix_valign), ������������ mmap/munmap.
        ��� ����� �����������, .so, ����������� LD_PRELOAD.
        Hoard -- ����� ����������� �����;
        SLAB -- ������������, ������� � �����������. Buddy allocator � libc ������, ������ ������ ����� 1/4 ������ (����� �������� � ������������� ������ ������).
        
        1) ������������� malloc, free, ��� ������ ����� � �������������� mmap, munmap;
        2) �������� ��������� -- ������� ������ ��������� ������;
        3) �� ��������� ������� SLAB -- �� �� �����, �� ��� ������� ������� ���� ������;
          
        4) ������� �� SLAB hoard ���������, ������ ���������� ������������.
        � ���������� �� ������ ���� init, finit: ��� PRELOAD ��� ����� �������� ������, init ����� ���� ������ ������ ����� ������� malloc.
        ����� ������� ������ -- ��� ������ malloc ���������, ���� �� ������������� ��� ���������.
        
    Pipeline
        
        ������ ����� ����� ������������� � ������������, � ������, � ������ ����.
        ������ ������: SRAN, DRAM
        
        //��� ��� ��� ������� ��� ��������, ���� etc.
        
        ������ ����� ������� ���� ���������� �����, ���� ����� ����.
        � ���������� ���� instruction pointer, �������� ��� ���������� ������������� ���������.
        Latch -- ������� -- ����� ��� �������� ��������� ��������� ��� �������� �� ���������,
        �� ������ ����� � �� �������� ������, �� ����� -- ������ � �� ����������, �� � ��
        ����� ������� ����������� ��������.
        
        "�������� �����" ������������:
        1 / performance = time / program =
                        = (instructions / program) * (cycles / instructions) * (time / cycle)
        ��� ����� ��������� ������ ������ ����������.
        ������ ����� -- ������������ ���������.
        ������ ����� -- ����������������.
        ��������� ����� -- ����������.
        
        performance = (IPC * freq) / (instructions per program)
        �������������, ��� ���������� ������������������ ����� ������� IPC, ������� ����������
        �����������, ������� ������� ��� �������� ������������ ���������.
        
        ��� ����� �������� �����������
        - Braniacs
        - Speed-demons
        - x86
        
        �������� pipeline:
        Fetch -> Decode -> Fetch args -> Execute -> Commit
        
        Braniacs: ���������� � ���������� �����, � ��������� ���� ����� ����� ����� ���������� �� �������.
        Speed-demons: ������� ������ pipeline'� �� �������� ���������� ������, ��������, branch prediction
            ���������� � ��������� ������ � ��� ����������� �� ��������� ����� predictor'��.
        x86: ���-�� ����������, ������������ ������� pipeline, �� �� ���������, ��� � speed-demon-�����������.
        
    Pipeline bubbles
    
        ��������: ������������� ��������� ����������� ���������� ����������.
        �� ������� ��������� ��������� ����� ���� ���������� ��� ���� ��� ��������� ������.
        ���� ����������: RAW, WAR, WAW.
        
        �������� ����� ������� -- � ����������� ����� ���� ��� �����:
        1) machine state -- ��������� �� ������ �����, ��� ������ ����� �������� ���������.
        2) acyclic state -- �������� ��������� ���������.
        ����� ���������, ����� �� ��������� ������ ������������ � ����� ������� ����� ����� �� acyclic state.
        �� ������ commit stage ��������� �������� ����������� �� (2) � (1).
        
    �������������� ��������
        
        ������ ���������� ���������, � �� ���� ���� ���������� ����� ����������� ��������� ����������.
        DIQ -- decoded instruction queue, � ��� �������� �������������� ���������� ������ � �����������,
        ����� �� �� �������� ������� ��� �������� �� �����-������ ALU, ����� ������������� ���������� ����������
        ����� ����� �������, �� ����� �� �������������� ������ � ���������.
        
        - branchings
        - ALU
        - vector instructions
        - FPU
        
        ����� ���������� ������� ��������� ���������� � reloads buffer, ������ �� ������������ � �� commit stage, �
        � decode stage, ����� ����� ���� ������������ ������ ��� ����������� ��������, ��� �� �������� �� commit stage
        (������ ����� ����������� ���� �� ��� �� �����).
        �� ������ ������������� ��� ���� �������� ��������� ����������, ����� �� ��������� �� �������� ����������.
        
        Output dependencies �������� � ������� ��������� �������� � acyclic stage ��� ��������� ����������.
        
    �����������
        
        ����� ���������������� ��������� �������:
        [.........|.....|.....] 
             ^       ^     ^ ����� ������ �������
             ^       ^ ���
             ^ "����" (����� ����� ��� ��������������� � ����� � ���� �����)
    
        �������:
         ___________________________
        |        |        |         |
        | n-k-m  | k ���  | 2^m ��� | + presence
        | addr   | tag    | data    |   bit
        |________|________|_________|
        |        |        |         |
        
        L1 -- ������ ��������� �������������, ��������������� ������ ������� ����������� ������������������ �� ������
        � �������, � ���������� ��������� �� �� ���� ���������, � ������ � ����������.
        ����������� ����������������� ������ ����� ����� ���������, ����� ������ ���������� ���������.
        
        L1D � L1I ��������� ��� ����, ����� ����������� ������ ���������� � �������� � �������.
        � speed-demon-����������� ��� �� �������, � �����������: ���� ��� ���������� ����, ������� ���������
        ������ ��� ������������ ������, � ������ ����� ������ ����, � ��� ����������� �� ��� ��� ����.
        
        L1 ����������� � L2, ���� � L1 �� ������� ��������, �� �������� CPU, ��� ������ �� ������, � ��� ���� ��������
        ����������� � L2. ���������� L2 ������ � L3.
        
        ������������� ����� � ����������� ������ ������� ����������� �������������.
        (������ "invalidate cache line").
        ��������, L1 �������� �������������, � �� ���� ������� ������, ����� ���� �������� ������ ����, ����� �� ���
        ������. ����� �������� ���-�� ����� � commit stage.
        
        L1 � L2 �������� � ����������� �������, ����� �� ������ ����� �� resolving.
        ������������� ���� � ����������� ������� ������������ ������.
        
        L3 ������ ��� �������� � ���������� �������, ����� ����� ���� ���������� ������������� � ����� ��� ���, ������������
            ��� ���������� TLB.
        ����� L2 � L3 ��������� TLB -- translation lookaside buffer, ��� resolving'� ���������� �������.
        �� PowerPC ������ � TLB �������������� ������������ ��������, � x86 -- ������������� �� muOps, � pagefault
            ������������ ������ ���� �� �������.
            
        L4 -- ������ ������� ����������� L3.
        
    ��������� ����������
    
        1) ��������� instruction pointer
        2) ��������� ����������
        
        � Intel Pentium � ������ ��� ����� ������ �� ������ ����.
        ���� ���������� ���������� � L1, �� � ������ ������ ��� ������ ����� �����������
            �� ������ �����.
            
        � ����������� ����������� ���������� ���������� �� �� �����, � ��������� �����, ��� �� � ������������,
            ��� ������ ����, ��� ��� ���������� �������.
            
        ���� ��� ���� ���������� program counter, � ��� �������� ����������� ��� ����������, ����������� �� ���������,
        � ���� ��� branching'� ���������� ������ ����� ��� instruction pointer'�, �� ������������ ����������, �������
        ������� � ��������� ��� ����������, � ������� ���������� program counter ������, ��� � ���� branching-����������.
        
    ������������������
    
        ���������� ������� ������������������ ��������� ���������� � "���������" (�������� ����������� ��������,
        �������� ������������ ���������� � �.�.) -- �� ���� branch prediction � L1I-����.
        
    Branch prediction
        
        1) �������, ����� �� �������
        2) ������� ����� ��������
        
        ������� � ������� �������: ������� ��� ���������� ��� ���� -- saturated counter.
        ����� �������� ����������� saturated counter.
        
        ���������: ������� ����� ������ �� ���������� ����������� ��������.
        ����� ���������� ������������ ������� (~8 ���, ��� ����� ��� ��������� ������ � ����������� �������) ��������
        �� �������������.
        
        ����� ����, ������ � �������� �� ����� �������� � ����� ������� ��������, � ��� ret ����� ���������
        ���������� ��������������� �� ����� �����.
        
        � speed-demon-����������� ���� ��������� branch predictor'�� � ������ �������� �������.
        � brainiac-����������� ������ ���� ������� branch predictor.
        
������ � ���� ����: ���� � branch prediction.

To read: The Effects of the x86 ISA on the Front End: Where have all the sycles gone?